from django.contrib import admin

from .models import ToDoItem, Event
# Register your models here.
admin.site.register(ToDoItem)
admin.site.register(Event)